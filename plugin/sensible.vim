" sensible.vim - sensible to mallicksm
" Maintainer:   Soummya Mallick
" Version:      1.0

if exists('g:loaded_sensible') || &compatible
  finish
else
  let g:loaded_sensible = 1
endif

if has('autocmd')
  filetype plugin indent on
endif
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif

"change directory to the file being edited
autocmd BufEnter * silent! lcd %:p:h

"Create my filetypes
autocmd Bufenter *.h                 set filetype=c
autocmd Bufenter *.elist             set filetype=elist
autocmd Bufenter *.tdf               set filetype=tdf
autocmd Bufenter *.csh               set filetype=csh
autocmd Bufenter *.qel               set filetype=qel
autocmd Bufenter *.bash_aliases      set filetype=sh
autocmd Bufenter *.bash_functions    set filetype=sh
autocmd Bufenter * colorscheme desert
autocmd Bufenter * set t_vb= "Unset visualbell

" Gvim Tab colors
autocmd Bufenter * hi TabLine guifg=LightGreen guibg=DarkGreen
autocmd Bufenter * hi TabLineFill  guifg=Blue guibg=Yellow
autocmd Bufenter * hi TabLineSel  guifg=Red guibg=Yellow
autocmd FileType make setlocal noexpandtab
" Use :help 'option' to see the documentation for the given option.
set expandtab
set nobackup noswapfile           " disable ~ and .swp file backup feature
set wildignore=.a,.o
set wildchar=<Tab>
set wildmenu
set wildmode=list:longest,full
set wildcharm=<C-Z>
nnoremap <F10> :b <C-Z>
vnoremap // y/<C-R>"<CR>

set visualbell 
set foldmethod=marker
set linebreak formatoptions=1 showbreak=>>>
set ignorecase smartcase
set backspace=indent,eol,start
set complete-=i
set autoindent
set tabstop=3 shiftwidth=3
set nrformats-=octal
set ttimeout
set ttimeoutlen=100
set incsearch hlsearch            
set nowrap
set wildignore+=*.a,*.o,.git,.svn*.tmp


set laststatus=2
set ruler
set showcmd

if !&scrolloff
  set scrolloff=1
endif
if !&sidescrolloff
  set sidescrolloff=5
endif
set display+=lastline

if &encoding ==# 'latin1' && has('gui_running')
  set encoding=utf-8
  set lines=50 columns=200
endif

if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j " Delete comment character when joining commented lines
endif

if has('path_extra')
  setglobal tags-=./tags tags^=./tags;
endif

if &shell =~# 'fish$'
  set shell=/bin/bash
endif

set autoread
set fileformats+=mac

if &history < 1000
  set history=10000
endif
if &tabpagemax < 50
  set tabpagemax=50
endif
if !empty(&viminfo)
  set viminfo^=!
endif
set sessionoptions-=options

" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^linux'
  set t_Co=16
endif

" Load matchit.vim, but only if the user hasn't installed a newer version.
if !exists('g:loaded_matchit') && findfile('plugin/matchit.vim', &rtp) ==# ''
  runtime! macros/matchit.vim
endif

" Set cursorline and cursorcolumn on windows
"au WinLeave * set nocursorline nocursorcolumn
"au WinEnter * set cursorline cursorcolumn
"set cursorline cursorcolumn

set background=light
"cabbrev e tabe
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif

let g:airline_theme='aurora'
" unicode symbols
let g:airline_left_sep = '▶'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.whitespace = ''
let g:airline_symbols.paste = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.crypt = ''
let g:airline_symbols.spell = ''
let g:airline_symbols.notexists = ''
let g:airline_section_y = ''
let g:airline_section_x = ''
let g:airline_section_error = ''
let g:airline_section_warning = ''
let g:airline_mode_map = {
    \ '__' : '-',
    \ 'n'  : 'N',
    \ 'i'  : 'I',
    \ 'R'  : 'R',
    \ 'c'  : 'C',
    \ 'v'  : 'V',
    \ 'V'  : 'V',
    \ '' : 'V',
    \ 's'  : 'S',
    \ 'S'  : 'S',
    \ '' : 'S',
    \ }


" Keyboad Mappings
" Wrap toggle
map <leader>w :set wrap!<CR>
" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif
inoremap <C-U> <C-G>u<C-U>
"
"Tab Navigation
nnoremap <C-PageUp>       :tabp<CR>
nnoremap <C-PageDown>     :tabn<CR>
"
"Split Navigation
nnoremap <leader>n        <C-w>ww
nnoremap <leader>h        <C-w>h
nnoremap <leader>l        <C-w>l
nnoremap <leader>j        <C-w>j
nnoremap <leader>k        <C-w>k
nnoremap <leader>x        :!xterm -geom 120x60+1400+1000 -fa 6x9 -fs 8 -bg yellow -fg black -e "cd $PWD && /bin/bash" &
"
"Redefine j k to jump into wrapped lines
nmap j gj
nmap k gk
"
"Cycle through buffers
nmap <leader><leader>n :bnext<cr>
nmap <leader><leader>p :bprev<cr>
"
"Logical window splits
set splitbelow
set splitright
"
"File Tab   Navigation
nnoremap <leader>f        viW<C-w>gf
nnoremap <leader>gf       viW<C-w>gf
nnoremap gf               viW<C-w>gf
"
"File Split Navigation
nnoremap <leader><C-f>    <C-w>f
nnoremap <leader>q        <ESC>:q<cr>
"
"Nerdtree Settings
nmap <leader>nt :NERDTreeToggle<CR>
let g:NERDTreeWinSize=40
let NERDTreeIgnore=['\.o$', '\.d$', '\.git$']
let NERDTreeBookmarksSort=0
let g:NERDTreeShowBookmarks=1
let NERDTreeShowLineNumbers=1
"Tagbar Settings
let g:tagbar_sort = 0
let g:tagbar_compact = 1
"let g:tagbar_autopreview = 1
nmap <leader>tt :TagbarToggle<CR>
nmap <leader>rs :call ReloadAllSnippets()<CR>
"
"ultisnips configuration
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetsDir="~/.vim/bundle/vim-sensible/UltiSnips"
"Utility maps
:nmap <leader>s :source $MYVIMRC<CR>
:nmap <leader>v :tabe $MYVIMRC<CR>
