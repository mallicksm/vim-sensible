# sensible.vim

This great plugin has been modified by mallicksm for his personal use (from Tim Pope).

Think of this as your .vimrc file in a bundle form.

* You can install this with vundle.
* Your configuration is backed up in the cloud
* Configuring a new Machine or VM is a snap.

## Installation
###Install Vundle first

    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

###Then Modify your .vimrc to reflect the change shown below

    set nocompatible
    filetype off
    set rtp+=~/.vim/bundle/vundle/
    
    call vundle#begin()
    Plugin 'gmarik/vundle'
    Plugin 'https://bitbucket.org/mallicksm/vim-sensible'
    call vundle#end()

    filetype plugin indent on

## Features

See the [source][] for the authoritative list of features.  (Don't worry, it's
mostly `:set` calls.)  Here's a taste:
[source]: https://bitbucket.org/mallicksm/vim-sensible/src/d596d73fd79650a4751af80f2c9d624e07d2cff7/plugin/sensible.vim?at=master
## FAQ

> How can I see what this plugin actually does?

The [source][] is authoritative.  Use `:help 'option'` to see the
documentation for an option.  If you install [scriptease.vim][], you can press
`K` on an option (or command, or function) to jump to its documentation.

[scriptease.vim]: https://github.com/tpope/vim-scriptease

> How can I override a setting?

Just clone this repository and make it your own. The idea is to never have a local .vimrc file with anything more than vundle calls

## Contributing

I want this to be a plugin nobody objects to cloning.  Everything is negotiable.

Feel free to ask a question if you're not sure why I've set something, or change it.

## License
Copyright © Soummya Mallick.  Distributed under the same terms as Vim itself.
See `:help license`.
Copyright © Tim Pope.  Distributed under the same terms as Vim itself.
See `:help license`.
